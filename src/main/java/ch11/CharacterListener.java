package ch11;

public interface CharacterListener {
  public void newCharacter(CharacterEvent ce);
}
