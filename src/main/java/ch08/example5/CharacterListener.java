package ch08.example5;

public interface CharacterListener {
  public void newCharacter(CharacterEvent ce);
}
