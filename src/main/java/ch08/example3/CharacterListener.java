package ch08.example3;

public interface CharacterListener {
  public void newCharacter(CharacterEvent ce);
}
